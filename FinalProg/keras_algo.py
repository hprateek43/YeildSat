# -*- coding: utf-8 -*-
"""
Created on Fri May 25 11:32:13 2018

@author: GUR03132
"""

import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
import cv2
import os
from keras.models import model_from_json
from pathlib import Path
import pandas as pd


_TRAIN_DATA_FILE_NAME_ = './train.csv'
_TEST_DATA_FILE_NAME_ = './test.csv'

_TRAIN_DATA_FILE_NAME_ = './dom_class_train.csv'
_TEST_DATA_FILE_NAME_ = './dom_class_test.csv'

_MAKE_BACKGROUND_TRANSPARENT_PATH = './BackgroundTransparent/'

def read_tranparent_images(file_name):
    
    image = []
    label1 = []
    num_files1 = 0
    
    df = pd.DataFrame()
    df = pd.read_csv(file_name, encoding='utf-8')
    old_dir_path = os.getcwd()
    os.chdir(_MAKE_BACKGROUND_TRANSPARENT_PATH)
    for i, row in df.iterrows():
        file_id = row['Id']
        label_id = row['cluster']
        tmp = cv2.imread(str(file_id) + '.png')
#        tmp = crop_mask_img(tmp)
#        tmp = tmp/255
#        if tmp.size != 388:
#            print(tmp.shape)
#            print(tmp.size)
#            continue
#        print(len(label1))

        image.append(tmp)
        label1.append(int(label_id))
        num_files1 += 1
    imgRet = np.stack(image)
    label_train = np.asarray(label1)
    label_train  = label_train.reshape((num_files1,1))    
    label_train = keras.utils.to_categorical(label_train)
    print(label_train.shape)
        
    os.chdir(old_dir_path)    
    
    return imgRet,label_train
        




num_files = 0
def read_images(dirPath):
    image = []
    label1 = []
    num_files1 = 0
    print(dirPath)
    for root, dir, files in os.walk(dirPath):
        print(root)
        print(files)
        if(root == dirPath):
            num_files1 = len(files)
            for file in files:
                print(file.split(".")[0].split("_")[0])
                print(file.split(".")[0].split("_")[1])
                label1.append(file.split(".")[0].split("_")[1])
                tmp = cv2.imread(dirPath + file)
                tmp = crop_mask_img(tmp)
                tmp = tmp/255
                print(tmp.shape)
                print(len(label1))
                image.append( tmp)
    imgRet = np.stack(image)
    label_train = np.asarray(label1)
    label_train  = label_train.reshape((num_files1,1))    
    label_train = keras.utils.to_categorical(label_train)
    print(label_train.shape)
            
    return imgRet,label_train


def create_model():
    
    '''
    model = Sequential([
            Conv2D(32, (3, 3), activation='relu', input_shape=(310, 386, 3)),
            Conv2D(32, (3, 3), activation='relu'),
#            Conv2D(256, (3, 1), activation='relu'),
#            Conv2D(256, (3, 2), activation='relu'),
#            Conv2D(512, (3, 1), activation='relu'),
#            Conv2D(512, (3, 2), activation='relu'),
#            Conv2D(1024, (3, 1), activation='relu'),
            Flatten(),
            Dense(256, activation='relu'),
            Dropout(0.5),
            Dense(101, activation='softmax')
            ])
    '''
    
    
    model = Sequential([
            #Conv2D(32, (3, 3), activation='relu', input_shape=(310, 386, 3)),
            Conv2D(32, (3, 3), activation='relu', input_shape=(34, 34, 3)),
            #Conv2D(32, (3, 3), activation='relu'),
            MaxPooling2D(pool_size=(2, 2)),
            Conv2D(64, (3, 3), activation='relu'),
            MaxPooling2D(pool_size=(2, 2)),
            Dropout(0.25),
            Flatten(),
            Dense(128, activation='relu'),
            Dropout(0.5),
            Dense(10, activation='softmax')
            ])
    
    return model


def store_model(model):
    model_json = model.to_json()
    with open(modelPath, "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(modelWtPath)
    
def load_model():
    json_file = open(modelPath , 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(modelWtPath)
    print("Loaded model from disk")
    return loaded_model    


def mask_img(image):
    b = image[:,:,0]
    g = image[:,:,1]
    r = image[:,:,2]
    print("b with 255:",b[b == 255].size)
    print("g with 255:",g[g == 255].size)
    print("r with 255:",r[r == 255].size)

    r[r > 110] = 255
    mska = np.ma.masked_greater(r,110)
    np.copyto(g,r,where=mska.mask)
    np.copyto(b,r,where=mska.mask)
    tst = np.dstack((b,g,r))
    #cv2.imwrite("masked.jpg",tst)
    #cv2.imshow("Final",tst)
    #cv2.waitKey(0)
    return tst

def crop_mask_img(img):
    crop_img = img[228:657, 724:1180]
    image = mask_img(crop_img)
    return image

filesPath ="D:\\Hackathon\\FinalProg\\BackgroundTransparent\\"
modelPath = "D:\\Hackathon\\FinalProg\\CNN\\Models\\model.json"
modelWtPath = "D:\\Hackathon\\FinalProg\\CNN\\Models\\model.h5"

my_file = Path(modelPath)
x_train,y_train = read_tranparent_images(_TRAIN_DATA_FILE_NAME_)
x_test,y_test = read_tranparent_images(_TEST_DATA_FILE_NAME_)


lossFunc = 'categorical_crossentropy'
learning_rate = 0.001
decay_rate = learning_rate / 1000
momentum = 0.7
sgdopt = SGD(lr=learning_rate, decay=decay_rate, momentum=momentum, nesterov=False)

if (not my_file.is_file()):
    print("Saved model to disk")
    model = create_model()
    model.compile(loss=lossFunc, metrics=['accuracy'],optimizer=sgdopt)
    print("x_train shape{}:  y_train shape:{}".format(x_train.shape,y_train.shape))
    model.fit(x_train, y_train, batch_size=3, epochs=10)
    store_model(model)
else:
    model = load_model()
    model.compile(loss=lossFunc, metrics=['accuracy'],optimizer=sgdopt)
score = model.evaluate(x_test, y_test, batch_size=3)
print("Score:",score)
predictRes  = model.predict(x_test,verbose=1  )
print("Result type:{}  Result shape:{}".format(type(predictRes),predictRes.shape))
def myfunc(x):
    return x.argmax()
print(np.apply_along_axis(myfunc,axis=1,arr=predictRes))
print(np.apply_along_axis(myfunc,axis=1,arr=y_test))
   