# -*- coding: utf-8 -*-
"""
Created on Wed May 30 16:34:13 2018

@author: GUR29990
"""

import cv2
import glob
import os
import numpy as np
from scipy.signal import convolve2d
import math
import pandas as pd



_ORIG_SAT_IMAGES_PATH = './SatImages/'
_RESIZED_SAT_IMAGES_PATH = './ResizeSatImages/'
_CROPPED_SAT_IMAGES_PATH = './CroppedImages/'
_MAKE_BACKGROUND_TRANSPARENT_PATH = './BackgroundTransparent/'
_FEATURE_EXTRACTION_FILE_NAME_ = './image_feature_data.csv'

_PRED_ORIG_SAT_IMAGES_PATH = './Pred/SatImages/'
_PRED_RESIZED_SAT_IMAGES_PATH = './Pred/ResizeSatImages/'
_PRED_CROPPED_SAT_IMAGES_PATH = './Pred/CroppedImages/'
_PRED_MAKE_BACKGROUND_TRANSPARENT_PATH = './Pred/BackgroundTransparent/'
_PRED_FEATURE_EXTRACTION_FILE_NAME_ = './Pred/image_feature_data.csv'


def estimate_noise(I):
  I = I.sum(-1)
  H, W = I.shape

  M = [[1, -2, 1],
       [-2, 4, -2],
       [1, -2, 1]]

  sigma = np.sum(np.sum(np.absolute(convolve2d(I,M))))
  sigma = sigma * math.sqrt(0.5 * math.pi) / (6 * (W-2) * (H-2))

  return sigma


def _extract_feature_attributes_from_cropped_images_(featExtPath,bgTransaprentPath):
    #_FEATURE_EXTRACTION_FILE_NAME_,_MAKE_BACKGROUND_TRANSPARENT_PATH
    #fopen = open(_FEATURE_EXTRACTION_FILE_NAME_, 'w')
    fopen = open(featExtPath, 'w')
    
    old_dir_path = os.getcwd()
    #os.chdir(_MAKE_BACKGROUND_TRANSPARENT_PATH)
    os.chdir(bgTransaprentPath)
    
    final_df = pd.DataFrame()
#    df_data_list = []
    hist_col_name_list = []

    all_img = glob.glob('*.png')
    for counter in range(0, len(all_img)):
        print(all_img[counter])
        line = ''
        img = cv2.imread(all_img[counter], cv2.IMREAD_COLOR)
        max_channels = np.amax([np.amax(img[:,:,0]), np.amax(img[:,:,1]), np.amax(img[:,:,2])])
        
        image = cv2.imread(all_img[counter], cv2.IMREAD_COLOR)
        hist_red = cv2.calcHist([image], [0], None, [256], [0,256])
        hist_green = cv2.calcHist([image], [1], None, [256], [0,256])
        hist_blue = cv2.calcHist([image], [2], None, [256], [0,256])
#        print(hist)
        
        line = str(all_img[counter].split('.')[0]) + ',' + str(np.amin(img[:,:,0])) + ','
        line += str(np.amin(img[:,:,1]))
        line += ','
        line += str(np.amin(img[:,:,2]))
        line += ','
        line += str(np.amax(img[:,:,0]))
        line += ','
        line += str(np.amax(img[:,:,1]))
        line += ','
        line += str(np.amax(img[:,:,2]))
        line += ','
        line += str(estimate_noise(img))
        line += ','
        
        hist_col_name_list_red = []
        hist_col_name_value = []
        for cnt in range(0, len(hist_red)):
            hist_col_name_list_red.append('hist_red_'+ str(cnt)) 
            hist_col_name_value.append(float(hist_red[cnt][0]))
        hist_col_name_value = ['{:.2f}'.format(x) for x in hist_col_name_value]
        line += str(','.join(hist_col_name_value))
        line += ','
        
        hist_col_name_list_green = []
        hist_col_name_value = []
        for cnt in range(0, len(hist_green)):
            hist_col_name_list_green.append('hist_green_'+ str(cnt)) 
            hist_col_name_value.append(float(hist_green[cnt][0]))
        hist_col_name_value = ['{:.2f}'.format(x) for x in hist_col_name_value]
        line += str(','.join(hist_col_name_value))
        line += ','
        
        hist_col_name_list_blue = []
        hist_col_name_value = []
        for cnt in range(0, len(hist_blue)):
            hist_col_name_list_blue.append('hist_blue_'+ str(cnt)) 
            hist_col_name_value.append(float(hist_blue[cnt][0]))
        hist_col_name_value = ['{:.2f}'.format(x) for x in hist_col_name_value]
        line += str(','.join(hist_col_name_value))

        line += '\n'
        fopen.write(line)
#        df_data_list.append(line)
    os.chdir(old_dir_path)
#    print(df_data_list)
    fopen.close()
    
    colnames_final = []
    colnames = ['Id', 'B-Low', 'G-Low', 'R-Low', 'B-High', 'G-High', 'R-High', 'Noise']
    colnames_final.extend(colnames)
    colnames_final.extend(hist_col_name_list_red)
    colnames_final.extend(hist_col_name_list_green)
    colnames_final.extend(hist_col_name_list_blue)
    print(colnames_final)
    #final_df = pd.read_csv(_FEATURE_EXTRACTION_FILE_NAME_, names=colnames_final, header=None)
    final_df = pd.read_csv(featExtPath, names=colnames_final, header=None)
    
    #final_df.to_csv(_FEATURE_EXTRACTION_FILE_NAME_, encoding="utf-8", index=False)
    final_df.to_csv(featExtPath, encoding="utf-8", index=False)
    

def _make_black_background_as_transparent_(cropImgPath,bgTransparentImgPath):
    #_CROPPED_SAT_IMAGES_PATH,_MAKE_BACKGROUND_TRANSPARENT_PATH
    old_dir_path = os.getcwd()
    #os.chdir(_CROPPED_SAT_IMAGES_PATH)
    os.chdir(cropImgPath)
    
    all_img = glob.glob('*.jpg')
    print(all_img)
    for counter in range(0, len(all_img)):
        img = cv2.imread(all_img[counter], cv2.IMREAD_UNCHANGED)
        print("Img type read:",all_img[counter],type(img))
        
        if(img.shape != (34,34,3)):
            print("img shape:{}  {}".format(all_img[counter],img.shape))    
        #img *= 1./255
        tmp = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        print("Tmp shape:",tmp.shape)
        _,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
        b, g, r = cv2.split(img)
        rgba = [b,g,r, alpha]
        dst = cv2.merge(rgba,4)
#        cv2.imshow("Edges", dst)
#        cv2.waitKey(0)

        os.chdir(old_dir_path)
        #os.chdir(_MAKE_BACKGROUND_TRANSPARENT_PATH)
        os.chdir(bgTransparentImgPath)
        #print(all_img[counter])
        #print("dst shape:",dst.shape)
        cv2.imwrite(all_img[counter].split('.')[0] + '.png', dst)
        os.chdir(old_dir_path)
        #os.chdir(_CROPPED_SAT_IMAGES_PATH)
        os.chdir(cropImgPath)
    os.chdir(old_dir_path)


def _resize_sat_images_(origImgPath,resizeSatImgPath):

    old_dir_path = os.getcwd()
    #os.chdir(_ORIG_SAT_IMAGES_PATH)
    os.chdir(origImgPath)
    
    all_img = glob.glob('*.jpg')
    for counter in range(0, len(all_img)):
        img = cv2.imread(all_img[counter])
        crop_img = img[228:657, 724:1180]
        #cv2.imshow("Edges", crop_img)
        #cv2.waitKey(0)
        os.chdir(old_dir_path)
        #os.chdir(_RESIZED_SAT_IMAGES_PATH)
        os.chdir(resizeSatImgPath)
        #print(all_img[counter])
        cv2.imwrite(all_img[counter], crop_img)
        os.chdir(old_dir_path)
        #os.chdir(_ORIG_SAT_IMAGES_PATH)
        os.chdir(origImgPath)
    os.chdir(old_dir_path)
        

def _crop_fields_from_images_(cropImgPath,resizeSatImgPath):

    old_dir_path = os.getcwd()
    #os.chdir(_RESIZED_SAT_IMAGES_PATH)
    os.chdir(resizeSatImgPath)
    print("Creeating NOAA Data")
    noaa_data = open('../input.csv','w+')
    noaa_data.write('no,uniqid,zip,year,month,day'+'\n')
    
    image_num = 1
    cnt  = 1
    all_img = glob.glob('*.jpg')
    print(os.getcwd())
    for counter in range(0, len(all_img)):
        print(all_img[counter])
        date = all_img[counter].split('.')[0].split('-')
        print(date)
        line='2000,' + str(cnt) +',89316,'+date[0]+','+date[1]+','+date[2]
        cnt += 1
        noaa_data.write(line+'\n')
        print(all_img[counter])
        print(os.getcwd())
        # load the image, clone it for output, and then convert it to grayscale
        image = cv2.imread(all_img[counter])
        
        orig = image.copy()
        output = image.copy()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        #cv2.imshow('1. Grayed image',gray)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
        
        # detect circles in the image
        circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 2,2,param1=30,param2=30,minRadius=16,maxRadius=19)
        #print(circles)
        # Create mask
        height,width = image.shape[:2]
        mask = np.zeros((height,width), np.uint8)
        
        
        # ensure at least some circles were found
        if circles is not None:
            #print(circles)
            # convert the (x, y) coordinates and radius of the circles to integers
            circles = np.round(circles[0, :]).astype("int")
         
            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circles:
                # draw the circle in the output image, then draw a rectangle
                # corresponding= to the center of the circle
                
                output = orig.copy()
                cv2.circle(output, (x, y), r, (0, 255, 0), 2)
                mask = np.zeros((height,width), np.uint8)
                cv2.circle(mask,(x,y),r,(255,255,255),-1,8,0)
             
                image = orig.copy()
                test = cv2.bitwise_and(image,image,mask = mask)
                os.chdir(old_dir_path)
                #os.chdir(_CROPPED_SAT_IMAGES_PATH)
                os.chdir(cropImgPath)
                crop_img = test[y-r:y+r, x-r:x+r]
                crop_img = cv2.resize(crop_img, dsize=(34, 34), interpolation=cv2.INTER_CUBIC)
                cv2.imwrite(str(image_num) + '.jpg', crop_img)
                os.chdir(old_dir_path)
                #os.chdir(_RESIZED_SAT_IMAGES_PATH)
                os.chdir(resizeSatImgPath)
                image_num += 1 
            os.chdir(old_dir_path)
            os.chdir(resizeSatImgPath)
    noaa_data.close()
    print("NOAA Datapoints Generated")
    #os.chdir(_RESIZED_SAT_IMAGES_PATH)
    #os.chdir(resizeSatImgPath)
    os.chdir(old_dir_path)

        
            
        
        
def image_proc_main(action):
    
    if(action == 'MODEL'):
        _resize_sat_images_(_ORIG_SAT_IMAGES_PATH,_RESIZED_SAT_IMAGES_PATH)
        _crop_fields_from_images_(_CROPPED_SAT_IMAGES_PATH,_RESIZED_SAT_IMAGES_PATH)
        #Crop only fields TBD
        _make_black_background_as_transparent_(_CROPPED_SAT_IMAGES_PATH,_MAKE_BACKGROUND_TRANSPARENT_PATH)
        _extract_feature_attributes_from_cropped_images_(_FEATURE_EXTRACTION_FILE_NAME_,_MAKE_BACKGROUND_TRANSPARENT_PATH)
    elif(action == 'PREDICT'):    
    
        _resize_sat_images_(_PRED_ORIG_SAT_IMAGES_PATH,_PRED_RESIZED_SAT_IMAGES_PATH)
        _crop_fields_from_images_(_PRED_CROPPED_SAT_IMAGES_PATH,_PRED_RESIZED_SAT_IMAGES_PATH)
        #Crop only fields TBD
        _make_black_background_as_transparent_(_PRED_CROPPED_SAT_IMAGES_PATH,_PRED_MAKE_BACKGROUND_TRANSPARENT_PATH)
        _extract_feature_attributes_from_cropped_images_(_PRED_FEATURE_EXTRACTION_FILE_NAME_,_PRED_MAKE_BACKGROUND_TRANSPARENT_PATH)
        
    #_resize_sat_images_()
    #_crop_fields_from_images_()
    #_make_black_background_as_transparent_()
    #_extract_feature_attributes_from_cropped_images_(_FEATURE_EXTRACTION_FILE_NAME_,_MAKE_BACKGROUND_TRANSPARENT_PATH)
    #MLP,MNB,SGB TBD
        
if __name__ == '__main__':
    image_proc_main('MODEL')        
        
