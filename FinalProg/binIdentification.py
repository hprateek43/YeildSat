# import the necessary packages
import numpy as np
import argparse
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image")
args = vars(ap.parse_args())

# load the image
image = cv2.imread(args["image"])
image = cv2.resize(image,(int(500),int(500)))

hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
#cv2.imshow("HSV",hsv)
#mask = cv2.inRange(hsv, (36, 0, 0), (70, 255,255))

## slice the green
#imask = mask>0
#green = np.zeros_like(hsv, np.uint8)
#green[imask] = hsv[imask]

#cv2.imwrite("green.jpg", green)
# define the list of boundaries
boundaries = [
        ([0, 101, 0], [255, 141, 255]),
        ([0, 162, 0], [255, 172, 255]),
        ([0, 122, 0], [255, 132, 255]),
        ([0, 149, 0], [255, 159, 255]),
        ([0, 153, 0], [255, 163, 255]),
        ([0, 149, 0], [255, 159, 255]),
        ([0, 168, 0], [255, 178, 255]),
        ([0, 131, 0], [255, 141, 255]),
        ([0, 137, 0], [255, 147, 255]),
        ([0, 110, 0], [255, 120, 255])
]

# loop over the boundaries
for (lower, upper) in boundaries:
        # create NumPy arrays from the boundaries
        lower = np.array(lower, dtype = "uint8")
        upper = np.array(upper, dtype = "uint8")

        # find the colors within the specified boundaries and apply
        # the mask
        mask = cv2.inRange(image, lower, upper)
        output = cv2.bitwise_and(image, image, mask = mask)

        # show the images
        cv2.imshow("images", np.hstack([image, output]))
        cv2.waitKey(0)
