from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import numpy as np 
import cv2
import numpy
import glob
import os
import pandas as pd
import shutil
import os
import glob
import math

##print(np.sqrt(4))

_LABELLED_DATA_PATH_ = './KNNLabelled/'
_CROPPED_SAT_IMAGES_PATH = './BackgroundTransparent/'
_MASTER_IMAGE_NAME_ = './MasterImage/masterImage.png'
_MASTER_IMAGE_CLASSIFIER_DATA_DUMP_FILE_NAME_ = './MasterImage/classifier.csv'

_MASTER_FILE_NAME_ = './dom_master_file_name.csv'
_TRAIN_DATA_FILE_NAME_ = './dom_train.csv'
_TEST_DATA_FILE_NAME_ = './dom_test.csv'

classifier = ''

def _delete_directory_and_its_contents_(dir_name):
    try:
        shutil.rmtree(dir_name)
    except OSError as e:
        print("Error: %s - %s." % (e.filename,e.strerror))
            
def _creation_of_directories_(path):
    try:
       os.stat(path)
    except:
       try:
           os.makedirs(path)
       except OSError as e:
           if e.errno != errno.EEXIST:
              raise

def find_nearest(array, value):
    #array = [i[1] for i in array]
    array = np.asarray(array)
    #print(array)
    #print(value)
    #idx = (np.abs(array - value)).argmin()
    b2=value[0]
    g2=value[1]
    r2=value[2]

    d_init = np.sqrt(((r2-array[0][0])*1)**3.5 + ((g2-array[0][0])*1)**5.0 + ((b2-array[0][0])*1)**1.5)
    pos_init = 0
    for z in range(1,len(array)-1):
        b1=array[z][0]
        g1=array[z][1]
        r1=array[z][2]

        d = np.sqrt(((r2-r1)*0.3)**2 + ((g2-g1)*0.59)**2 + ((b2-b1)*0.11)**2)
        #print("Dsitance: ",d)
        if d < d_init:
            d_init = d
            pos_init = z
    return pos_init


def _create_master_image_():
    dir = "./BackgroundTransparent/" # current directory
    ext = ".png" # or PNG for our transparent images
    pathname = os.path.join(dir, "*" + ext)
    images = [cv2.imread(img) for img in glob.glob(pathname)]
    
    height = sum(image.shape[0] for image in images)
    width = max(image.shape[1] for image in images)
    output = numpy.zeros((height,width,3))
    
    y = 0
    counter = 1
    for image in images:
        
        counter += 1
        h,w,d = image.shape
        output[y:y+h,0:w] = image
        y += h
        
    cv2.imwrite(_MASTER_IMAGE_NAME_, output)



def centroid_histogram(clt):
	# grab the number of different clusters and create a histogram
	# based on the number of pixels assigned to each cluster
	numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
	(hist, _) = np.histogram(clt.labels_, bins = numLabels)
 
	# normalize the histogram, such that it sums to one
	hist = hist.astype("float")
	hist /= hist.sum()
 
	# return the histogram
	return hist 

def plot_colors(hist, centroids):
	# initialize the bar chart representing the relative frequency
	# of each of the colors
	bar = np.zeros((50, 300, 3), dtype = "uint8")
	startX = 0
 
	# loop over the percentage of each cluster and the color of
	# each cluster
	for (percent, color) in zip(hist, centroids):
		# plot the relative percentage of each cluster
		endX = startX + (percent * 300)
		cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
			color.astype("uint8").tolist(), -1)
		startX = endX
	
	# return the bar chart
	return bar

## construct the argument parser and parse the arguments
#ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--image", required = True, help = "Path to the image")
#ap.add_argument("-c", "--clusters", required = True, type = int,
#	help = "# of clusters")
#args = vars(ap.parse_args())
 
def _classifier_generation_():
    global classifier
    _create_master_image_()
    # load the image and convert it from BGR to RGB so that
    # we can dispaly it with matplotlib
    image = cv2.imread(_MASTER_IMAGE_NAME_)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
     
    # show our image
    plt.figure()
    plt.axis("off")
    plt.imshow(image)
    
    # reshape the image to be a list of pixels
    image = image.reshape((image.shape[0] * image.shape[1], 3))
    # cluster the pixel intensities
    #clt = KMeans(n_clusters = args["clusters"])
    clt = KMeans(n_clusters = 11)
    clt.fit(image)
    # build a histogram of clusters and then create a figure
    # representing the number of pixels labeled to each color
    hist = centroid_histogram(clt)
    rgb_array = clt.cluster_centers_
    classifier = rgb_array
    classifier = sorted(classifier,key=lambda x: x[1])
    bar = plot_colors(hist, classifier)
    cv2.imwrite('./MasterImage/Classifier_BGR.jpg', bar)
    image = cv2.cvtColor(bar, cv2.COLOR_BGR2RGB)
    cv2.imwrite('./MasterImage/Classifier_RGB.jpg', image)
    #print(clt.cluster_centers_)
    ##print(np.unique(clt.labels_))
    ##print(type(rgb_array))
    #print("Classifier : "+str(classifier))
    
    fopen = open(_MASTER_IMAGE_CLASSIFIER_DATA_DUMP_FILE_NAME_, 'w')
    for cnt in range(0, len(rgb_array)):
        line = ''
        for inner_cnt in range(0, len(rgb_array[cnt])):
            line = line + str(rgb_array[cnt][inner_cnt]) + ','
        line = line + str(cnt) + '\n'
        fopen.write(line)
    fopen.close()
    
    
    final_df = pd.DataFrame()
    colnames = ['Red', 'Green', 'Blue', 'Cluster']
    final_df = pd.read_csv(_MASTER_IMAGE_CLASSIFIER_DATA_DUMP_FILE_NAME_, names=colnames, header=None)
    
    for i, row in final_df.iterrows():
        red_val = int(row['Red'])
        green_val = int(row['Green'])
        blue_val = int(row['Blue'])
        sum_val = red_val + green_val + blue_val
        if sum_val <= 6:
            break
    final_df = final_df.drop(final_df.index[i])
    final_df = final_df.reset_index(drop=True)
    for i, row in final_df.iterrows():
        final_df.at[i, 'Cluster'] = int(i)
        
    final_df[colnames] = final_df[colnames].applymap(np.int64)
    final_df.to_csv(_MASTER_IMAGE_CLASSIFIER_DATA_DUMP_FILE_NAME_, encoding="utf-8", index=False)    

    # show our color bart
    plt.figure()
    plt.axis("off")
    plt.imshow(bar)
    plt.show()
    
def plot_colors2(hist, centroids):
    bar = np.zeros((50, 300, 3), dtype="uint8")
    startX = 0

    for (percent, color) in zip(hist, centroids):
        
        # plot the relative percentage of each cluster
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
                      color.astype("uint8").tolist(), -1)
        startX = endX

    # return the bar chart
    return bar    
    
def find_histogram(clt):
    """
    create a histogram with k clusters
    :param: clt
    :return:hist
    """
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins=numLabels)

    hist = hist.astype("float")
    hist /= hist.sum()

    return hist

    
def _find_dominant_color():
    old_dir_path = os.getcwd()
    #os.chdir(_CROPPED_SAT_IMAGES_PATH)
    
    #    all_img = glob.glob('*.jpg')
    #    for counter in range(0, len(all_img)):    
    #        shutil.copy(all_img[counter], '../'+_LABELLED_DATA_PATH_)
        
    #    os.chdir(old_dir_path)
    #    os.chdir(_LABELLED_DATA_PATH_)
    all_img = glob.glob(os.getcwd()+'\\BackgroundTransparent\\*.png')
    #print('OAS Walk Ended')

    for counter in range(0, len(all_img)):   
        img = cv2.imread(all_img[counter])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        img = img.reshape((img.shape[0] * img.shape[1],3)) #represent as row*column,channel number
        clt = KMeans(n_clusters=10) #cluster number
        clt.fit(img)
        hist = find_histogram(clt)
        ##print(type(hist))
        ind = np.argmax(hist)
        ##print(ind)
        dominant = sorted(clt.cluster_centers_,key=lambda x: x[1])
        bar = plot_colors2(hist, dominant)
        ##print(dominant[len(dominant)-1][1])
        ##print(find_nearest(classifier,dominant[len(dominant)-1][1]))
        #plt.axis("off")
        #plt.imshow(bar)
        #plt.show()
        #print('OAS Walk Started') 
        #file_id = int(all_img[counter].split('.')[0])
        #cluster_id = find_nearest(classifier,dominant[len(dominant)-1][1])
        cluster_id = find_nearest(classifier,dominant[len(dominant)-1])

        #print('OAS File Started')

        #print(cluster_id)
        #print(_LABELLED_DATA_PATH_ + 'Label_' + str(cluster_id)+'/')
        shutil.copy(os.getcwd()+'\\BackgroundTransparent\\'+str(counter+1)+'.png', os.getcwd()+'\\KNNLabelled\\' + 'Label_' + str(cluster_id)+'\\'+str(counter+1)+'.png')
        #print('OAS Fle end Started')

def _create_test_train_data_for_dominant_class_():

    fopen = open(_MASTER_FILE_NAME_, 'w')
    line = ''
    z=[]
    counter = 0

#    os.rename(_LABELLED_DATA_PATH_+'Label_10', _LABELLED_DATA_PATH_+ 'Label_0')
    for root, dir, files in os.walk(_LABELLED_DATA_PATH_):
        #old_dir_path = os.getcwd()
        z = dir
        #print(z)
        print(root)
#        print(dir)
        for i in range(len(files)-1):
            file_name = str(files[i].split('.')[0]+','+ str(counter))+'\n'
            fopen.write(file_name)
#            print(file_name)
            #print(z)
        #print('###########')  
        if root != _LABELLED_DATA_PATH_:
            counter=counter+1
    fopen.close()
    col_name = [ 'Id', 'cluster' ]
    final_df = pd.read_csv(_MASTER_FILE_NAME_, names=col_name, header=None)
    final_df.to_csv(_MASTER_FILE_NAME_, encoding="utf-8", index=False)
    msk = np.random.rand(len(final_df)) < 0.8
    train = final_df[msk]
    test = final_df[~msk]
    print("Trained Data Shape :: {}".format(train.shape))
    print("Test Data Shape :: {}".format(test.shape))
    #labels = train['cluster'].values
    train.to_csv(_TRAIN_DATA_FILE_NAME_, encoding="utf-8", index=False)
    test.to_csv(_TEST_DATA_FILE_NAME_, encoding="utf-8", index=False)



    
def main():
    
    _delete_directory_and_its_contents_(_LABELLED_DATA_PATH_)
    _creation_of_directories_(_LABELLED_DATA_PATH_)
    for i in range(0, 10):
        _creation_of_directories_(_LABELLED_DATA_PATH_ + 'Label_' + str(i+1))
        
    _classifier_generation_()
    _find_dominant_color()
    
    _create_test_train_data_for_dominant_class_()
    
if __name__ == '__main__':
    main()