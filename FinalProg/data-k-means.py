# -*- coding: utf-8 -*-
"""
Created on Thu May 31 07:50:30 2018

@author: GUR29990
"""

from sklearn.cluster import KMeans
from sklearn.externals import joblib
import pandas as pd
import shutil
import os
import glob
import numpy as np


_FEATURE_EXTRACTION_FILE_NAME_ = './image_feature_data.csv'
_LABEL_RECORDS_MAPPING_FILE_NAME_ = './Id_Cluster_Mapping.csv'
_FINAL_FEATURE_DATA_FILE_NAME_ = './image_feature_data_final.csv'
_LABELLED_DATA_PATH_ = './LabeledData/'
_CROPPED_SAT_IMAGES_PATH = './CroppedImages/'
_TRAIN_DATA_FILE_NAME_ = './feat_train.csv'
_TEST_DATA_FILE_NAME_ = './feat_test.csv'


def _creation_of_directories_(path):
    try:
       os.stat(path)
    except:
       try:
           os.makedirs(path)
       except OSError as e:
           if e.errno != errno.EEXIST:
              raise

              
def _delete_directory_and_its_contents_(dir_name):
    try:
        shutil.rmtree(dir_name)
    except OSError as e:
        print("Error: %s - %s." % (e.filename,e.strerror))
            

def _rename_orig_images_as_per_cluster_():
    df = pd.DataFrame()
    df = pd.read_csv(_LABEL_RECORDS_MAPPING_FILE_NAME_, encoding='utf-8')
    print(df)
    
    df = df.astype(int)
    print(df.head())
    
    
    _delete_directory_and_its_contents_(_LABELLED_DATA_PATH_)
    _creation_of_directories_(_LABELLED_DATA_PATH_)
    for i in range(0, 10):
        _creation_of_directories_(_LABELLED_DATA_PATH_ + 'Label_' + str(i))
    
    old_dir_path = os.getcwd()
    os.chdir(_CROPPED_SAT_IMAGES_PATH)
    
#    all_img = glob.glob('*.jpg')
#    for counter in range(0, len(all_img)):    
#        shutil.copy(all_img[counter], '../'+_LABELLED_DATA_PATH_)
    
#    os.chdir(old_dir_path)
#    os.chdir(_LABELLED_DATA_PATH_)
    all_img = glob.glob('*.jpg')
    for counter in range(0, len(all_img)):    
         file_id = int(all_img[counter].split('.')[0])
#         print(file_id)
         tempdf = df[df['Id'] == file_id]
         cluster_id = int(tempdf.values.tolist()[0][1])
         shutil.move(all_img[counter], '../'+_LABELLED_DATA_PATH_ + 'Label_' + str(cluster_id)+'/'+all_img[counter])
#         print(tempdf.values.tolist()[0][1])
         
    
    os.chdir(old_dir_path)        

def create_clusters_of_features_():
    
    df = pd.DataFrame()
    df = pd.read_csv(_FEATURE_EXTRACTION_FILE_NAME_, encoding='utf-8')
    df = df.astype(float)
    
    id_list = []
    id_list = df['Id'].tolist()
    df = df.drop('Id', 1)

    #Convert to a matrix
    df = df.values
    
    
    num_clusters = 10
    km = KMeans(n_clusters=num_clusters)
    km.fit(df)
    clusters = km.labels_.tolist()
    #print(clusters)
    print(len(clusters))
#    joblib.dump(km,  'doc_cluster.pkl')

#    km = joblib.load('doc_cluster.pkl')
    clusters = km.labels_.tolist()
    #print(clusters)
    print(len(clusters))
    table_name = { 'Id': id_list, 'cluster': clusters }
    frame = pd.DataFrame(table_name)
    print(frame.head())
    frame.to_csv(_LABEL_RECORDS_MAPPING_FILE_NAME_,  encoding="utf-8", index=False)

    #_FINAL_FEATURE_DATA_FILE_NAME_
    df = pd.read_csv(_FEATURE_EXTRACTION_FILE_NAME_, encoding='utf-8')
    frame = pd.read_csv(_LABEL_RECORDS_MAPPING_FILE_NAME_, encoding='utf-8')
    new_df = pd.DataFrame()
    new_df = pd.merge(df, frame, on='Id')
    new_df.to_csv(_FINAL_FEATURE_DATA_FILE_NAME_,  encoding="utf-8", index=False)
    
    #Merge the data frames
    msk = np.random.rand(len(new_df)) < 0.8
    train = new_df[msk]
    test = new_df[~msk]
    print("Trained Data Shape :: {}".format(train.shape))
    print("Test Data Shape :: {}".format(test.shape))
    #labels = train['cluster'].values
    train.to_csv(_TRAIN_DATA_FILE_NAME_, encoding="utf-8", index=False)
    test.to_csv(_TEST_DATA_FILE_NAME_, encoding="utf-8", index=False)
    
    
def data_k_means():    
    create_clusters_of_features_()

    _rename_orig_images_as_per_cluster_()

if __name__ == '__main__':
    data_k_means()        
    
